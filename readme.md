To convert a directory of video files to h264, run

`./convert.sh path/to/source/ path/to/destination/`

To convert a specific file, run

`./convert.sh path/to/file.ext path/to/destination/`

- The output filename will be the input filename
- Files are automatically overwritten
