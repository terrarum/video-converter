#!/bin/zsh

# Setup.
inputIsDir=true
inputIsFile=false
cpuThreads=$(getconf _NPROCESSORS_ONLN)
excludedCodecs=('' 'ansi' 'mjpeg' 'png')

# Inputs.
inputPath=$1
outputPath=$2

timestamp() {
  date +%s
}

humanTime() {
  h=$(($1 / 3600))
  m=$((($1 / 60) % 60))
  s=$(($1 % 60))
  printf "%02d:%02d:%02d\n" $h $m $s
}

validate_inputs() {
  valid=true

  if [ -d "$inputPath" ]; then
    echo "$inputPath is a directory"
  elif [ -f "$inputPath" ]; then
    echo "$inputPath is a file"
    inputIsDir=false
    inputIsFile=true
  else
    echo "Cannot access input path."
    valid=false
  fi

  if [ ! -d "$outputPath" ]; then
    echo "Cannot access output directory, attempting to create."
    mkdir "$outputPath";
  fi

  if [ "$valid" = false ]; then
    echo "Exiting."
    exit 1
  fi
  printf "\nInputs are valid.\n"
}

get_codec() {
  ffprobe -loglevel quiet -select_streams v:0 -show_entries stream=codec_name -of default=noprint_wrappers=1:nokey=1 "$1"
}

reencode() {
  filePath=$1
  filename=$(basename "$1")
  codec=$(get_codec "$1")

#  if [ "$codec" != "h264" ]; then
    echo "Converting '$filename' from $codec to h264."
    # Hide banner, only show warnings, copy audio, tuned to film, will automatically overwrite target file.
    ffmpeg -hide_banner -loglevel error -i "$1" -threads "$cpuThreads" -map_chapters 0 -c:s copy -c:a copy -c:v libx264 -preset medium -tune animation -movflags +faststart -y "${2}/${filename%.*}.mkv"
#  else
#    echo "'$filename' is already $codec, copying to $outputPath."
#    rsync -zh --progress "$filePath" "$outputPath"
#    # rsync -rlptgoDzh --progress "$filePath" "$outputPath"
#  fi
}

# Go.

validate_inputs

if [ "$inputIsFile" = true ]; then
  reencode "$inputPath" "$outputPath"
fi

if [ "$inputIsDir" = true ]; then
  files=()
  while IFS= read -r -d '' file; do
    codec=$(get_codec "$file")

    codecAllowed=true
    for excludedCodec in "${excludedCodecs[@]}"; do
      if [ "$codec" = "$excludedCodec" ]; then
        codecAllowed=false
      fi
    done

    if [ "$codecAllowed" = false ] ; then
      echo "Excluding $file, reported codec '$codec' is in exclusion list."
    else
      files+=("$file")
    fi

  done < <(find "$inputPath" -type f -print0)

  printf "\nFiles to process:\n"
  printf "%s\n" "${files[@]}"
  echo ""

  totalToProcess=${#files[@]}
  remainingToProcess=${#files[@]}
  currentlyProcessing=1

  folderStartTime=$(timestamp)

  for file in "${files[@]}"; do
    fileStartTime=$(timestamp)

    echo "$(date +"%T") - Processing $currentlyProcessing of $totalToProcess";

    reencode "$file" "$outputPath"

    file_dir=$(dirname "$file")
    file_name=$(basename "$file")
    mv "$file" "$file_dir/$file_name"

    prettyDuration=$(($(timestamp) - fileStartTime))
    printf "File completed in %s.\n" "$(humanTime "$prettyDuration")"

    folderDuration=$(($(timestamp) - folderStartTime))
    remainingToProcess=$((remainingToProcess - 1))

    folderRemaining=$((remainingToProcess*folderDuration/currentlyProcessing))
    folderRemainingHuman=$(humanTime "$folderRemaining")
    estimatedCompletionTime=$(($(timestamp) + folderRemaining))
    estimatedCompletionTimeHuman=$(date -d @${estimatedCompletionTime} +%T)

    if [ "$currentlyProcessing" = "$totalToProcess" ]; then
      echo ""
      printf "Done!\n%s files completed in %s at %s." "$totalToProcess" "$(humanTime "$folderDuration")" "$estimatedCompletionTimeHuman"
    else
      folderDurationHuman=$(humanTime "$folderDuration")
      printf "Completed %s files in %s.\n" "$currentlyProcessing" "$folderDurationHuman"
      printf "Estimate remaining %s files will complete in %s at %s.\n" "$remainingToProcess" "$folderRemainingHuman" "$estimatedCompletionTimeHuman"
    fi

    currentlyProcessing=$((currentlyProcessing+1))
    echo ""
  done
fi
